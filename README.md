# iTunes Explorer

iTunes Explorer is an application that searches multimedia content in the iTunes Search API.

It has been built applying TDD.

The UX can be improved a lot, it is not a final user implementation at all, please feel free to ask for any modifications or better implementation and I'll look into it.

The app architecture has been implemented with a sort of IDD (https://vimeo.com/130256611) and Model View Presenter. In order to keep the view separate from the logic, the presenter has all the business logic and the view is as dummy as possible.

The dependencies management tool is Cocoapods (1.6.0) and the dependencies are: 

    - RxAlamofire
    - SwiftyJSON
    - SDWebImage
    - RxTest
    - RxBlocking
    
The networking framework I used was RXAlamofire just because I wanted to try it -- I'd never used it before this project and I thought it was a great oportunity to do it. The network client is behind a protocol so it's easy to switch to any other networking framework.

I decided to use Rx because it is a great tool that more and more developers are adopting.