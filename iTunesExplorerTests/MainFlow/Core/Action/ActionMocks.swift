//
//  ActionMocks.swift
//  iTunesExplorerTests
//
//  Created by Lucas Serruya on 13/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

@testable import iTunesExplorer
import RxSwift

class GetMoviesStub: GetMoviesAction {
    var executeCalledCount = 0
    
    func execute(searchText: String) -> Single<[ITunesMovie]> {
        executeCalledCount += 1
        return Single.just(ITunesSearchResultsFactory.createExpectedMoviesResult())
    }
}

class GetMusicStub: GetMusicAction {
    var executeCalledCount = 0
    
    func execute(searchText: String) -> Single<[ITunesMusic]> {
        executeCalledCount += 1
        return Single.just(ITunesSearchResultsFactory.createExpectedMusicResult())
    }
}

class GetTvShowsStub: GetTvShowsAction {
    var executeCalledCount = 0
    
    func execute(searchText: String) -> Single<[ITunesTvShow]> {
        executeCalledCount += 1
        return Single.just(ITunesSearchResultsFactory.createExpectedTvShowsResult())
    }
}
