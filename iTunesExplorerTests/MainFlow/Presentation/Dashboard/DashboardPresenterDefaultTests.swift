//
//  DashboardPresenterDefaultTests.swift
//  iTunesExplorerTests
//
//  Created by Lucas Serruya on 13/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

@testable import iTunesExplorer
import XCTest
import RxSwift

class DashboardPresenterDefaultTests: XCTestCase {
    
    var view: DashboardViewStub!
    var presenter: DashboardPresenterDefault!
    var getMoviesStub: GetMoviesStub!
    var getMusicStub: GetMusicStub!
    var getTvShowsStub: GetTvShowsStub!
    var coordinatorStub: MainFlowCoordinatorStub!
    
    func test_onPlayerOpenDashboard() {
        givenAViewStub()
        givenActionStubs()
        givenACoordinatorStub()
        givenAPresenter()
        
        whenViewIsReady()
        
        thenFirstSearchDetailsAreRequested()
    }
    
    func test_onPlayerPerformAMoviesSearch() {
        givenAViewStub()
        givenActionStubs()
        givenACoordinatorStub()
        givenAPresenter()
        
        presenter.onPlayerPerformAMoviesSearch(searchText: "U2")
        
        XCTAssertEqual(view.showLoadingCalledCount, 1)
        XCTAssertEqual(getMoviesStub.executeCalledCount, 1)
        XCTAssertEqual(view.hideLoadingCalledCount, 1)
        XCTAssertEqual(view.showMoviesCalledCount, 1)
    }
    
    func test_onPlayerPerformAMusicSearch() {
        givenAViewStub()
        givenActionStubs()
        givenACoordinatorStub()
        givenAPresenter()
        
        presenter.onPlayerPerformAMusicSearch(searchText: "U2")
        
        XCTAssertEqual(view.showLoadingCalledCount, 1)
        XCTAssertEqual(getMusicStub.executeCalledCount, 1)
        XCTAssertEqual(view.hideLoadingCalledCount, 1)
        XCTAssertEqual(view.showMusicCalledCount, 1)
    }
    
    func test_onPlayerPerformATvShowsSearch() {
        givenAViewStub()
        givenActionStubs()
        givenACoordinatorStub()
        givenAPresenter()
        
        presenter.onPlayerPerformATvShowSearch(searchText: "U2")
        
        XCTAssertEqual(view.showLoadingCalledCount, 1)
        XCTAssertEqual(getTvShowsStub.executeCalledCount, 1)
        XCTAssertEqual(view.hideLoadingCalledCount, 1)
        XCTAssertEqual(view.showTvShowsCalledCount, 1)
    }
    
    func test_onPlayerOpenPreview() {
        givenAViewStub()
        givenActionStubs()
        givenACoordinatorStub()
        givenAPresenter()
        
        presenter.onPlayerOpenPreview(url: "-")
        
        XCTAssertEqual(coordinatorStub.presentPreviewCalledCount, 1)
    }
    
    private func givenAViewStub() {
        view = DashboardViewStub()
    }
    
    private func givenActionStubs() {
        getMoviesStub = GetMoviesStub()
        getMusicStub = GetMusicStub()
        getTvShowsStub = GetTvShowsStub()
    }
    
    private func givenACoordinatorStub() {
        coordinatorStub = MainFlowCoordinatorStub()
    }
    
    private func givenAPresenter() {
        presenter = DashboardPresenterDefault(getMovies: getMoviesStub, getMusic: getMusicStub, getTvShows: getTvShowsStub, coordinator: coordinatorStub)
        presenter.view = view
    }
    
    fileprivate func whenViewIsReady() {
        presenter.onViewReady()
    }
    
    fileprivate func thenFirstSearchDetailsAreRequested() {
        XCTAssertEqual(view.askForFirstSearchDetailsCalledCount, 1)
    }
}

class DashboardViewStub: DashboardView {
    var askForFirstSearchDetailsCalledCount = 0
    var showLoadingCalledCount = 0
    var hideLoadingCalledCount = 0
    var showMoviesCalledCount = 0
    var showMusicCalledCount = 0
    var showTvShowsCalledCount = 0
    
    func setFocusOnSearchBar() {
        askForFirstSearchDetailsCalledCount += 1
    }
    
    func showMovies(movies: [ITunesMovie]) {
        showMoviesCalledCount += 1
    }
    
    func showMusic(music: [ITunesMusic]) {
        showMusicCalledCount += 1
    }
    
    func showTvShows(tvShows: [ITunesTvShow]) {
        showTvShowsCalledCount += 1
    }
    
    func showLoading() {
        showLoadingCalledCount += 1
    }
    
    func hideLoading() {
        hideLoadingCalledCount += 1
    }
}

class MainFlowCoordinatorStub: MainFlowCoordinator {
    var loadGameMainFlowNavBarCalledCount = 0
    var presentPreviewCalledCount = 0
    func loadGameMainFlowNavBarController() -> UINavigationController {
        loadGameMainFlowNavBarCalledCount += 1
        return UINavigationController()
    }
    
    func presentMultimediaPreview(url: String) {
        presentPreviewCalledCount += 1
    }
}
