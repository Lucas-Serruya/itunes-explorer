//
//  ITunesSearchResultsFactory.swift
//  iTunesExplorerTests
//
//  Created by Lucas Serruya on 12/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

@testable import iTunesExplorer

class ITunesSearchResultsFactory {
    static func createExpectedMoviesResult() -> [ITunesMovie] {
        return [ITunesMovie(trackName: "track name movie 1", artworkUrl00: "artworkUrl100 movie 1", longDescription: "long description movie 1", previewUrl: "preview url movie 1"), ITunesMovie(trackName: "track name movie 2", artworkUrl00: "artworkUrl100 movie 2", longDescription: "long description movie 2", previewUrl: "preview url movie 2")]
    }
    
    static func createExpectedMusicResult() -> [ITunesMusic] {
        return [ITunesMusic(trackName: "track name song 1", artistName: "artist name song 1", artworkUrl00: "artworkUrl100 song 1", previewUrl: "preview url song 1"), ITunesMusic(trackName: "track name song 2", artistName: "artist name song 2", artworkUrl00: "artworkUrl100 song 2", previewUrl: "preview url song 2")]
    }
    
    static func createExpectedTvShowsResult() -> [ITunesTvShow] {
        return [ITunesTvShow(trackName: "track name show 1", artistName: "artist name show 1", artworkUrl00: "artworkUrl100 show 1", longDescription: "long description show 1", previewUrl: "preview url show 1"), ITunesTvShow(trackName: "track name show 2", artistName: "artist name show 2", artworkUrl00: "artworkUrl100 show 2", longDescription: "long description show 2", previewUrl: "preview url show 2")]
    }
}
