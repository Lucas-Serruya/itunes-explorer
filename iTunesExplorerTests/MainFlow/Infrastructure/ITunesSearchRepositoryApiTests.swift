//
//  ITunesSearchRepositoryApiTests.swift
//  iTunesExplorerTests
//
//  Created by Lucas Serruya on 09/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

@testable import iTunesExplorer
import XCTest
import RxSwift
import RxBlocking

class ITunesSearchRepositoryApiTests: XCTestCase {
    
    let expectedBasePath = "https://itunes.apple.com/search"
    static let expectedSearchText = "U2"
    let expectedGetMoviesParameters = ["term": expectedSearchText, "media": "movie"]
    let expectedGetMusicParameters = ["term": expectedSearchText, "media": "music"]
    let expectedGetTvShowsParameters = ["term": expectedSearchText, "media": "tvShow"]
    
    let networkClientStub = NetworkClientStub()
    var repository: ItunesSearchRepositoryApi!
    var lastMoviesReceived: MaterializedSequenceResult<[ITunesMovie]>!
    var lastMusicReceived: MaterializedSequenceResult<[ITunesMusic]>!
    var lastTvShowsReceived: MaterializedSequenceResult<[ITunesTvShow]>!
    
    func test_onMoviesSearchExecuted_shouldCallSearchEndpoint() {
        givenAnITunesSearchRepository()
        
        whenGetMovies()
        
        thenMovieSearchPathAndParametersAreCorrect()
    }
    
    func test_onMusicSearchExecuted_shouldCallSearchEndpoint() {
        givenAnITunesSearchRepository()
        
        whenGetMusic()
        
        thenMusicSearchPathAndParametersAreCorrect()
    }
    
    func test_onTvShowsSearchExecuted_shouldCallSearchEndpoint() {
        givenAnITunesSearchRepository()
        
        whenGetTvShows()
        
        thenTvShowsSearchPathAndParametersAreCorrect()
    }
    
    func test_onGetMoviesExecuted_shouldReturnExpectedMovie() {
        givenAResponseForRequests(fromFile: "itunes_search_movies_response")
        givenAnITunesSearchRepository()
        
        whenGetMovies()
        
        
        thenMoviesAreRetrieved()
    }
    
    func test_onGetMusicExecuted_shouldReturnExpectedMusic() {
        givenAResponseForRequests(fromFile: "itunes_search_music_response")
        givenAnITunesSearchRepository()
        
        whenGetMusic()
        
        thenMusicIsRetrieved()
    }
    
    func test_onGetTvShowsExecuted_shouldReturnExpectedMusic() {
        givenAResponseForRequests(fromFile: "itunes_search_tv_shows_response")
        givenAnITunesSearchRepository()
        
        whenGetTvShows()
        
        thenTvShowsAreRetrieved()
    }
    
    func thenMoviesAreRetrieved() {
        switch lastMoviesReceived! {
        case .completed(let moviesReceived):
            let moviesReceived = moviesReceived[0]
            XCTAssertEqual(moviesReceived, ITunesSearchResultsFactory.createExpectedMoviesResult())
        case .failed:
            XCTFail("Error finding movies")
        }
    }
    
    func thenMusicIsRetrieved() {
        switch lastMusicReceived! {
        case .completed(let lastMusicReceived):
            let lastMusicReceived = lastMusicReceived[0]
            XCTAssertEqual(lastMusicReceived, ITunesSearchResultsFactory.createExpectedMusicResult())
        case .failed:
            XCTFail("Error finding music")
        }
    }
    
    func thenTvShowsAreRetrieved() {
        switch lastTvShowsReceived! {
        case .completed(let lastTvShowsReceived):
            let lastTvShowsReceived = lastTvShowsReceived[0]
            XCTAssertEqual(lastTvShowsReceived, ITunesSearchResultsFactory.createExpectedTvShowsResult())
        case .failed:
            XCTFail("Error finding tv shows")
        }
    }
    
    func givenAnITunesSearchRepository() {
        repository = ItunesSearchRepositoryApi(networkClient: networkClientStub)
    }
    
    private func givenAResponseForRequests(fromFile fileName: String) {
        let jsonData = jsonDataResponseFromFile(fileName: fileName)
        networkClientStub.responseForRequests = jsonData
    }
    
    private func whenGetMovies() {
        lastMoviesReceived = repository.getMovies(searchText: ITunesSearchRepositoryApiTests.expectedSearchText).toBlocking().materialize()
    }
    
    private func whenGetMusic() {
        lastMusicReceived = repository.getMusic(searchText: ITunesSearchRepositoryApiTests.expectedSearchText).toBlocking().materialize()
    }
    
    private func whenGetTvShows() {
        lastTvShowsReceived = repository.getTvShows(searchText: ITunesSearchRepositoryApiTests.expectedSearchText).toBlocking().materialize()
    }
    
    private func thenMovieSearchPathAndParametersAreCorrect() {
        XCTAssertEqual(networkClientStub.lastReceivedParameters as! [String: String], expectedGetMoviesParameters)
        XCTAssertEqual(networkClientStub.lastUsedPath, expectedBasePath)
    }
    
    private func thenMusicSearchPathAndParametersAreCorrect() {
        XCTAssertEqual(networkClientStub.lastReceivedParameters as! [String: String], expectedGetMusicParameters)
        XCTAssertEqual(networkClientStub.lastUsedPath, expectedBasePath)
    }
    
    private func thenTvShowsSearchPathAndParametersAreCorrect() {
        XCTAssertEqual(networkClientStub.lastReceivedParameters as! [String: String], expectedGetTvShowsParameters)
        XCTAssertEqual(networkClientStub.lastUsedPath, expectedBasePath)
    }
    
    private func jsonDataResponseFromFile(fileName: String) -> Data {
        let bundle = Bundle(for: ITunesSearchRepositoryApiTests.self)
        let path = bundle.path(forResource: fileName, ofType: "json")!
        return try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
    }
}


