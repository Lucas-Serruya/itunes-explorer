//
//  NetworkClientStub.swift
//  iTunesExplorerTests
//
//  Created by Lucas Serruya on 12/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

@testable import iTunesExplorer
import RxSwift
import SwiftyJSON

class NetworkClientStub: NetworkCLient {
    var lastUsedPath: String?
    var lastReceivedParameters: [String : Any]?
    var responseForRequests: Data?
    
    func get(path: String, parameters: [String : Any]?) -> Single<JSON> {
        lastUsedPath = path
        lastReceivedParameters = parameters
        if let responseForRequests = responseForRequests {
            return Single.just(JSON(responseForRequests))
        }
        
        return Single.error(NSError(domain: "default_error", code: 1, userInfo: nil))
    }
}
