//
//  GradientView.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 17/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//  Code extracted from https://stackoverflow.com/questions/8164609/is-there-a-way-to-make-gradient-background-color-in-the-interface-builder

import UIKit

@IBDesignable
final class GradientView: UIView {
    @IBInspectable var startColor: UIColor = UIColor.clear
    @IBInspectable var endColor: UIColor = UIColor.clear
    
    var gradient: CAGradientLayer?
    
    override func awakeFromNib() {
        addGradient()
    }
    
    func addGradient() {
        gradient = CAGradientLayer()
        setGradientFrame()
        gradient?.colors = [startColor.cgColor, endColor.cgColor]
        gradient?.zPosition = -1
        
        if let gradient = self.gradient {
            layer.addSublayer(gradient)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setGradientFrame()
    }
    
    private func setGradientFrame() {
        gradient?.frame = CGRect(x: 0,
                                 y: 0,
                                 width: self.frame.size.width,
                                 height: self.frame.size.height)
    }
}
