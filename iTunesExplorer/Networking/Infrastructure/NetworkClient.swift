//
//  DashboardView.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 06/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import RxAlamofire
import Foundation
import RxSwift
import SwiftyJSON

protocol NetworkCLient {
    func get(path: String, parameters: [String: Any]?) -> Single<JSON>
}

class ReactiveNetworkClient: NetworkCLient {
    let session = URLSession.shared
    let diposeBag = DisposeBag()
    
    func get(path: String, parameters: [String : Any]?) -> Single <JSON> {
        return RxAlamofire.requestData(.get, path, parameters: parameters).map({ (arg) -> JSON in
            let (_, response) = arg
            return try JSON(data: response)
        }).asSingle()
    }
}
