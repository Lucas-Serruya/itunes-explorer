//
//  GetTvShow.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 13/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import RxSwift

class GetTvShows: GetTvShowsAction {
    let repository: ItunesSearchRepository
    
    init (repository: ItunesSearchRepository) {
        self.repository = repository
    }
    
    func execute(searchText: String) -> Single<[ITunesTvShow]> {
        return repository.getTvShows(searchText:searchText)
    }
}

