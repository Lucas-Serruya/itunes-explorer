//
//  GetMovies.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 13/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import RxSwift

class GetMovies: GetMoviesAction {
    let repository: ItunesSearchRepository
    
    init (repository: ItunesSearchRepository) {
        self.repository = repository
    }
    
    func execute(searchText: String) -> Single<[ITunesMovie]> {
        return repository.getMovies(searchText: searchText)
    }
}
