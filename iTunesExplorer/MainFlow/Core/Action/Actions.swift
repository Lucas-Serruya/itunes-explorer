//
//  Actions.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 13/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import RxSwift

protocol GetMoviesAction {
    func execute(searchText: String) -> Single<[ITunesMovie]>
}

protocol GetMusicAction {
    func execute(searchText: String) -> Single<[ITunesMusic]>
}

protocol GetTvShowsAction {
    func execute(searchText: String) -> Single<[ITunesTvShow]>
}
