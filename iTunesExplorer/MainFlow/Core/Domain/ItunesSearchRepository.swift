//
//  ItunesSearchRepository.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 13/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import Foundation
import RxSwift

protocol ItunesSearchRepository {
    func getMovies(searchText: String) -> Single<[ITunesMovie]>
    func getMusic(searchText: String) -> Single<[ITunesMusic]>
    func getTvShows(searchText: String) -> Single<[ITunesTvShow]>
}
