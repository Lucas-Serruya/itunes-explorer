//
//  ITunesMovie.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 12/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

struct ITunesMovie: MultimediaResource, Equatable {
    let trackName: String
    let artworkUrl00: String
    let longDescription: String
    let previewUrl: String
    
    static func ==(lhs: ITunesMovie, rhs: ITunesMovie) -> Bool {
        return lhs.trackName == rhs.trackName && lhs.artworkUrl00 == rhs.artworkUrl00 && lhs.longDescription == rhs.longDescription && lhs.previewUrl == rhs.previewUrl
    }
}

