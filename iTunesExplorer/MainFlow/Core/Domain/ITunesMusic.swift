//
//  ITunesMovie.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 12/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

struct ITunesMusic: MultimediaResource, Equatable {
    let trackName: String
    let artistName: String
    let artworkUrl00: String
    let previewUrl: String
    
    static func ==(lhs: ITunesMusic, rhs: ITunesMusic) -> Bool {
        return lhs.trackName == rhs.trackName && lhs.artistName == rhs.artistName && lhs.artworkUrl00 == rhs.artworkUrl00 && lhs.previewUrl == rhs.previewUrl
    }
}

