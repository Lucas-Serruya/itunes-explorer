//
//  MultimediaResource.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 13/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

protocol MultimediaResource {
    var trackName: String { get }
    var artworkUrl00: String { get }
    var previewUrl: String { get }
}
