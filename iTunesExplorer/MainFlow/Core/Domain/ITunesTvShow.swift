//
//  ITunesTvShow.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 12/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

struct ITunesTvShow: MultimediaResource, Equatable {
    let trackName: String
    let artistName: String
    let artworkUrl00: String
    let longDescription: String
    let previewUrl: String
    
    static func ==(lhs: ITunesTvShow, rhs: ITunesTvShow) -> Bool {
        return lhs.trackName == rhs.trackName && lhs.artistName == rhs.artistName && lhs.artworkUrl00 == rhs.artworkUrl00 && lhs.longDescription == rhs.longDescription && lhs.previewUrl == rhs.previewUrl
    }
}

