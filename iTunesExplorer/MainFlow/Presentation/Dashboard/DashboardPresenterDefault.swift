//
//  DashboardPresenterDefault.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 06/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import Foundation
import RxSwift

class DashboardPresenterDefault: DashboardPresenter {
    
    weak var view: DashboardView?
    let disposeBag = DisposeBag()
    
    let getMovies: GetMoviesAction
    let getMusic: GetMusicAction
    let getTvShows: GetTvShowsAction
    let coordinator: MainFlowCoordinator
    
    init(getMovies: GetMoviesAction, getMusic: GetMusicAction, getTvShows: GetTvShowsAction, coordinator: MainFlowCoordinator) {
        self.getMovies = getMovies
        self.getMusic = getMusic
        self.getTvShows = getTvShows
        self.coordinator = coordinator
    }
    
    func onViewReady() {
        view?.setFocusOnSearchBar()
    }
    
    func onPlayerPerformAMoviesSearch(searchText: String) {
        getMovies.execute(searchText: searchText).do(onSubscribe: {
           self.view?.showLoading()
        }, onDispose: {
            self.view?.hideLoading()
        }).subscribe(onSuccess: { (itunesMovies) in
            self.view?.showMovies(movies: itunesMovies)
        }) { (error) in
            
        }.disposed(by: disposeBag)
    }
    
    func onPlayerPerformAMusicSearch(searchText: String) {
        getMusic.execute(searchText: searchText).do(onSubscribe: {
            self.view?.showLoading()
        }, onDispose: {
            self.view?.hideLoading()
        }).subscribe(onSuccess: { (itunesMusic) in
            self.view?.showMusic(music: itunesMusic)
        }) { (error) in
        }.disposed(by: disposeBag)
    }
    
    func onPlayerPerformATvShowSearch(searchText: String) {
        getTvShows.execute(searchText: searchText).do(onSubscribe: {
            self.view?.showLoading()
        }, onDispose: {
            self.view?.hideLoading()
        }).subscribe(onSuccess: { (itunesTvShows) in
            self.view?.showTvShows(tvShows: itunesTvShows)
        }) { (error) in
        }.disposed(by: disposeBag)
    }
    
    func onPlayerOpenPreview(url: String) {
        coordinator.presentMultimediaPreview(url: url)
    }
}
