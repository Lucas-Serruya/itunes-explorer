//
//  MovieCell.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 13/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import UIKit
import SDWebImage

class MusicCell: UITableViewCell {
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatar.layer.cornerRadius = avatar.frame.height/2
        avatar.clipsToBounds = true
    }
    
    func showInfo(artistName: String, trackName: String, artworkUrl00: String) {
        self.trackName.text = trackName
        self.avatar.sd_setImage(with: URL(string: artworkUrl00), completed: nil)
        
        self.artistName.text = artistName
    }
}
