//
//  SearchResultsTableView.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 13/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import UIKit

enum SearchType: Int {
    case movies, music, tvShows
}

protocol SearchResultsTableViewDelegate: class {
    func didSelectMovieWithPreviewUrl(url:String)
    func didSelectMusicWithPreviewUrl(url:String)
    func didSelectTvShowWithPreviewUrl(url:String)
}

class SearchResultsTableView: UIView {
    
    enum CellIdentifiers: String {
        case MovieCell, MusicCell, TvShowCell
    }
    
    var tableView: UITableView?
    var searchType = SearchType.movies
    var items: [MultimediaResource]?
    let emptyCell = UITableViewCell(frame: CGRect())
    var currentContentInset: CGFloat = 0
    
    weak var delegate: SearchResultsTableViewDelegate?

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        createTableView()
    }
    
    private func createTableView() {
        initializeTableView()
        registerCellsOnTableView()
        customizeTableView()
        
        if let tableView = tableView {
            self.addSubview(tableView)
        }
    }
    
    private func initializeTableView() {
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    private func registerCellsOnTableView() {
        tableView?.register(UINib(nibName: CellIdentifiers.MovieCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.MovieCell.rawValue)
        tableView?.register(UINib(nibName: CellIdentifiers.MusicCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.MusicCell.rawValue)
        tableView?.register(UINib(nibName: CellIdentifiers.TvShowCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.TvShowCell.rawValue)
    }
    
    private func customizeTableView() {
        tableView?.contentInset = UIEdgeInsets(top: currentContentInset, left: 0, bottom: 0, right: 0)
        tableView?.separatorStyle = .none
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 200
    }
}

//behavior
extension SearchResultsTableView {
    func showMovies(movies: [ITunesMovie]) {
        searchType = .movies
        items = movies
        self.tableView?.reloadData()
        scrollToTop()
    }
    
    func showMusic(music: [ITunesMusic]) {
        searchType = .music
        items = music
        self.tableView?.reloadData()
        scrollToTop()
    }
    
    func showTvShows(tvShows: [ITunesTvShow]) {
        searchType = .tvShows
        items = tvShows
        self.tableView?.reloadData()
        scrollToTop()
    }
    
    func addTopInsetToTableView(insetToAdd: CGFloat) {
        currentContentInset += insetToAdd
        self.tableView?.contentInset = UIEdgeInsets(top: currentContentInset, left: 0, bottom: 0, right: 0)
    }
    
    private func scrollToTop() {
        guard let tableView = self.tableView else {
            return
        }
        
        if (tableView.numberOfRows(inSection: 0) > 0) {
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
    }
}


extension SearchResultsTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch searchType {
        case .movies:
            if let movie = items?[indexPath.row] as? ITunesMovie {
                delegate?.didSelectMovieWithPreviewUrl(url: movie.previewUrl)
            }
        case .music:
            if let song = items?[indexPath.row] as? ITunesMusic {
                delegate?.didSelectMovieWithPreviewUrl(url: song.previewUrl)
            }
        case .tvShows:
            if let show = items?[indexPath.row] as? ITunesTvShow {
                delegate?.didSelectTvShowWithPreviewUrl(url: show.previewUrl)
            }
        }
    }
}

extension SearchResultsTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch searchType {
        case .movies: return cellForMovie(indexPath: indexPath)
        case .music: return cellForSong(indexPath: indexPath)
        case .tvShows: return cellForTvShow(indexPath: indexPath)
        }
    }
    
    func cellForMovie(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView?.dequeueReusableCell(withIdentifier: CellIdentifiers.MovieCell.rawValue, for: indexPath) as? MovieCell
        let movie = items?[indexPath.row] as! ITunesMovie
        cell?.showInfo(trackName: movie.trackName, artworkUrl00: movie.artworkUrl00, longDescription: movie.longDescription)
        
        cell?.selectionStyle = .none
    
        return cell ?? emptyCell
    }
    
    func cellForSong(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView?.dequeueReusableCell(withIdentifier: CellIdentifiers.MusicCell.rawValue, for: indexPath) as? MusicCell
        let music = items?[indexPath.row] as! ITunesMusic
        cell?.showInfo(artistName: music.artistName, trackName: music.trackName, artworkUrl00: music.artworkUrl00)
        
        cell?.selectionStyle = .none
        
        return cell ?? emptyCell
    }
    
    func cellForTvShow(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView?.dequeueReusableCell(withIdentifier: CellIdentifiers.TvShowCell.rawValue, for: indexPath) as? TvShowCell
        let show = items?[indexPath.row] as! ITunesTvShow
        cell?.showInfo(artistName: show.artistName, trackName: show.trackName, longDescription: show.longDescription, artworkUrl00: show.artworkUrl00)
        
        cell?.selectionStyle = .none
        
        return cell ?? emptyCell
    }
}
