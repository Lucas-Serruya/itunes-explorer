//
//  DashboardViewController.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 06/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchResultsTableView: SearchResultsTableView!
    @IBOutlet weak var segmentedSelectorHeight: NSLayoutConstraint!
    @IBOutlet weak var searchHeaderHeight: NSLayoutConstraint!
    @IBOutlet weak var searchTypeSelectorHeight: NSLayoutConstraint!
    
    var presenter: DashboardPresenter
    var currentSearchType = SearchType.movies
    
    init(presenter: DashboardPresenter) {
        self.presenter = presenter
        
        super.init(nibName: "DashboardViewController", bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.onViewReady()
        
        searchResultsTableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func selectedSearchTypeDidChange(_ sender: UISegmentedControl) {
        guard let currentSearchType = SearchType(rawValue: sender.selectedSegmentIndex) else {
            return
        }
        
        self.currentSearchType = currentSearchType
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        let bottomSafeAreaInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0;
        
        searchResultsTableView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - bottomSafeAreaInset)
        
        searchResultsTableView.addTopInsetToTableView(insetToAdd: searchHeaderHeight.constant)
    }
}

extension DashboardViewController: DashboardView {
    @objc func setFocusOnSearchBar() {
        searchBar.becomeFirstResponder()
    }
    
    func showMovies(movies: [ITunesMovie]) {
        if (movies.count == 0) { showNoContentAlert() }
        searchResultsTableView.showMovies(movies: movies)
    }

    func showMusic(music: [ITunesMusic]) {
        if (music.count == 0) { showNoContentAlert() }
        searchResultsTableView.showMusic(music: music)
    }
    
    func showTvShows(tvShows: [ITunesTvShow]) {
        if (tvShows.count == 0) { showNoContentAlert() }
        searchResultsTableView.showTvShows(tvShows: tvShows)
    }
    
    func showLoading() {
        //implement
    }
    
    func hideLoading() {
        //implement
    }
    
    func showNoContentAlert() {
        let alert = UIAlertController(title: "Content Not Found",
                                      message: nil,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "Search again", style: .default) { (action) in
            self.setFocusOnSearchBar()
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion:nil)
    }
}

extension DashboardViewController: SearchResultsTableViewDelegate {
    func didSelectMovieWithPreviewUrl(url: String) {
        loadVideoPlayerWithUrl(url: url)
    }
    
    func didSelectMusicWithPreviewUrl(url: String) {
        loadVideoPlayerWithUrl(url: url)
    }
    
    func didSelectTvShowWithPreviewUrl(url: String) {
        loadVideoPlayerWithUrl(url: url)
    }
    
    private func loadVideoPlayerWithUrl(url:String) {
        presenter.onPlayerOpenPreview(url: url)
    }
}

extension DashboardViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text, searchText.count > 0 else {
            return
        }
        
        switch currentSearchType {
        case .movies:
            presenter.onPlayerPerformAMoviesSearch(searchText: searchText)
        case .music:
            presenter.onPlayerPerformAMusicSearch(searchText: searchText)
        case .tvShows:
            presenter.onPlayerPerformATvShowSearch(searchText: searchText)
        }
        
        searchBar.resignFirstResponder()
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        showSegmentedSelection()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        hideSegmentedSelection()
    }
    
    func showSegmentedSelection() {
        segmentedSelectorHeight.constant = searchTypeSelectorHeight.constant + 1
        
        searchResultsTableView.addTopInsetToTableView(insetToAdd: searchTypeSelectorHeight.constant)
        
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideSegmentedSelection() {
        segmentedSelectorHeight.constant = 0
        searchResultsTableView.addTopInsetToTableView(insetToAdd: -(searchTypeSelectorHeight.constant))
        
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
}
