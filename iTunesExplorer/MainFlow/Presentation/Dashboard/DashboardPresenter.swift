//
//  DashboardPresenter.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 06/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import Foundation

protocol DashboardPresenter {
    func onViewReady()
    func onPlayerPerformAMoviesSearch(searchText: String)
    func onPlayerPerformAMusicSearch(searchText: String)
    func onPlayerPerformATvShowSearch(searchText: String)
    func onPlayerOpenPreview(url: String)
}
