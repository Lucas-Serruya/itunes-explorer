//
//  DashboardView.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 06/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

protocol DashboardView: class {
    func setFocusOnSearchBar()
    func showMovies(movies:[ITunesMovie])
    func showMusic(music:[ITunesMusic])
    func showTvShows(tvShows:[ITunesTvShow])
    func showLoading()
    func hideLoading()
}
