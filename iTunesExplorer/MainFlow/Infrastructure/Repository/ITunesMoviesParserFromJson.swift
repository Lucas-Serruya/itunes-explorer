//
//  ITunesMoviesParserFromJson.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 12/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import SwiftyJSON

class ITunesMoviesParserFromJson {
    static let parsingError = NSError(domain: "generic_parsing_error", code: 1, userInfo: nil)
    
    static func getMoviesFromJson(jsonResponse: JSON) throws -> [ITunesMovie] {
        do {
            let itunesSearch = try ITunesMoviesSearchRawFromJson(jsonResponse)
            if let iTunesMovies = iTunesMoviesFromSearchResult(itunesSearch) {
                return iTunesMovies
            } else {
                throw parsingError
            }
        }
        catch let error {
            throw error
        }
    }
    
    static private func ITunesMoviesSearchRawFromJson(_ jsonResponse: JSON) throws -> ITunesMoviesSearchRaw {
        return try JSONDecoder().decode(ITunesMoviesSearchRaw.self, from: jsonResponse.rawData())
    }
    
    static private func iTunesMoviesFromSearchResult(_ itunesSearch: ITunesMoviesSearchRaw) -> [ITunesMovie]? {
        return itunesSearch.movies.map { (movieRaw) -> ITunesMovie in
            return iTunesMovieFromItunesMovieRaw(movieRaw)
        }
    }
    
    static private func iTunesMovieFromItunesMovieRaw(_ movieRaw: ITunesMovieRaw) -> ITunesMovie {
        return ITunesMovie(trackName: movieRaw.trackName, artworkUrl00: movieRaw.artworkUrl100, longDescription: movieRaw.longDescription, previewUrl: movieRaw.previewUrl)
    }
}
