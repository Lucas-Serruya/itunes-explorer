//
//  ItunesSearchRepositoryApi.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 12/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import Foundation
import RxSwift

class ItunesSearchRepositoryApi: ItunesSearchRepository {
    
    private let networkClient: NetworkCLient
    private let disposeBag = DisposeBag()
    
    private let basePath = "https://itunes.apple.com/search"
    private let mediaKeyword = "media"
    private let termKeyword = "term"
    
    init(networkClient: NetworkCLient) {
        self.networkClient = networkClient
    }
    
    func getMovies(searchText: String) -> Single<[ITunesMovie]> {
        let moviesSearchParams = [termKeyword:searchText, mediaKeyword: "movie"]
        return self.networkClient.get(path: basePath, parameters: moviesSearchParams).map { (response) -> [ITunesMovie] in
            return try ITunesMoviesParserFromJson.getMoviesFromJson(jsonResponse: response)
        }
    }
    
    func getMusic(searchText: String) -> Single<[ITunesMusic]> {
        let moviesSearchParams = [termKeyword:searchText, mediaKeyword: "music"]
        return self.networkClient.get(path: basePath, parameters: moviesSearchParams).map { (response) -> [ITunesMusic] in
            return try ITunesMusicParserFromJson.getMusicFromJson(jsonResponse: response)
        }
    }
    
    func getTvShows(searchText: String) -> Single<[ITunesTvShow]> {
        let moviesSearchParams = [termKeyword:searchText, mediaKeyword: "tvShow"]
        return self.networkClient.get(path: basePath, parameters: moviesSearchParams).map { (response) -> [ITunesTvShow] in
            return try ITunesTvShowParserFromJson.getTVShowsFromJson(jsonResponse: response)
        }
    }
}

