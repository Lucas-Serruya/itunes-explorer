//
//  ITunesMusicParserFromJson.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 13/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import SwiftyJSON

class ITunesMusicParserFromJson {
    static let parsingError = NSError(domain: "generic_parsing_error", code: 1, userInfo: nil)
    
    static func getMusicFromJson(jsonResponse: JSON) throws -> [ITunesMusic] {
        do {
            let itunesSearch = try ITunesMusicSearchRawFromJson(jsonResponse)
            if let iTunesMusic = iTunesMoviesFromSearchResult(itunesSearch) {
                return iTunesMusic
            } else {
                throw parsingError
            }
        }
        catch let error {
            throw error
        }
    }
    
    static private func ITunesMusicSearchRawFromJson(_ jsonResponse: JSON) throws -> ITunesMusicSearchRaw {
        return try JSONDecoder().decode(ITunesMusicSearchRaw.self, from: jsonResponse.rawData())
    }
    
    static private func iTunesMoviesFromSearchResult(_ itunesSearch: ITunesMusicSearchRaw) -> [ITunesMusic]? {
        return itunesSearch.music.map { (musicRaw) -> ITunesMusic in
            return iTunesMusicFromItunesMusicRaw(musicRaw)
        }
    }
    
    static private func iTunesMusicFromItunesMusicRaw(_ musicRaw: ITunesMusicRaw) -> ITunesMusic {
        return ITunesMusic(trackName: musicRaw.trackName, artistName: musicRaw.artistName, artworkUrl00: musicRaw.artworkUrl100, previewUrl: musicRaw.previewUrl)
    }
}
