//
//  ITunesTvShowParserFromJson.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 12/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import SwiftyJSON

class ITunesTvShowParserFromJson {
    static let parsingError = NSError(domain: "generic_parsing_error", code: 1, userInfo: nil)
    
    static func getTVShowsFromJson(jsonResponse: JSON) throws -> [ITunesTvShow] {
        do {
            let itunesSearch = try ITunesTVShowsSearchRawFromJson(jsonResponse)
            if let iTunesMovies = iTunesTVShowsFromSearchResult(itunesSearch) {
                return iTunesMovies
            } else {
                throw parsingError
            }
        }
        catch let error {
            throw error
        }
    }
    
    static private func ITunesTVShowsSearchRawFromJson(_ jsonResponse: JSON) throws -> ITunesTvShowSearchRaw {
        return try JSONDecoder().decode(ITunesTvShowSearchRaw.self, from: jsonResponse.rawData())
    }
    
    static private func iTunesTVShowsFromSearchResult(_ itunesSearch: ITunesTvShowSearchRaw) -> [ITunesTvShow]? {
        return itunesSearch.tvShows.map { (tvShowRaw) -> ITunesTvShow in
            return iTunesTVShowsFromItunesMovieRaw(tvShowRaw)
        }
    }
    
    static private func iTunesTVShowsFromItunesMovieRaw(_ tvShowRaw: ITunesTvShowRaw) -> ITunesTvShow {
        return ITunesTvShow(trackName: tvShowRaw.trackName, artistName: tvShowRaw.artistName, artworkUrl00: tvShowRaw.artworkUrl100, longDescription: tvShowRaw.longDescription, previewUrl: tvShowRaw.previewUrl)
    }
}
