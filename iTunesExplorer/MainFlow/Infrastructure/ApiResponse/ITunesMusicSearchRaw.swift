//
//  ITunesMoviesSearchRaw.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 07/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

struct ITunesMusicSearchRaw: Decodable {
    let resultCount: Int64
    let music: [ITunesMusicRaw]
    
    enum CodingKeys: String, CodingKey {
        case resultCount = "resultCount"
        case music = "results"
    }
}
