//
//  ITunesTvShowSearchRaw.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 07/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

struct ITunesTvShowSearchRaw: Decodable {
    let resultCount: Int64
    let tvShows: [ITunesTvShowRaw]
    
    enum CodingKeys: String, CodingKey {
        case resultCount = "resultCount"
        case tvShows = "results"
    }
}
