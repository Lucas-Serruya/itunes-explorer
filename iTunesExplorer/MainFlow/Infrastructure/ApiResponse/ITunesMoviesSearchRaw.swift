//
//  ITunesMoviesSearchRaw.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 07/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

struct ITunesMoviesSearchRaw: Decodable {
    let resultCount: Int64
    let movies: [ITunesMovieRaw]
    
    enum CodingKeys: String, CodingKey {
        case resultCount = "resultCount"
        case movies = "results"
    }
}
