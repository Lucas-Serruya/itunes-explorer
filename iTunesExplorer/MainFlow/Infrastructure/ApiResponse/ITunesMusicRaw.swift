//
//  ITunesMusicRaw.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 12/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

struct ITunesMusicRaw:Decodable {
    let trackName: String
    let artistName: String
    let artworkUrl100: String
    let previewUrl: String
    
    enum CodingKeys: String, CodingKey {
        case trackName = "trackName"
        case artistName = "artistName"
        case artworkUrl100 = "artworkUrl100"
        case previewUrl = "previewUrl"
    }
}

