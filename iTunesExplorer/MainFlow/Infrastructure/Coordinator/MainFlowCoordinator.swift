//
//  MainFlowCoordinator.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 06/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import UIKit

protocol MainFlowCoordinator {
    func loadGameMainFlowNavBarController() -> UINavigationController
    func presentMultimediaPreview(url: String)
}
