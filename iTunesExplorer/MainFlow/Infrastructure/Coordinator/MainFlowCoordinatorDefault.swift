//
//  MainFlowCoordinatorDefault.swift
//  iTunesExplorer
//
//  Created by Lucas Serruya on 06/02/2019.
//  Copyright © 2019 lds. All rights reserved.
//

import UIKit
import AVKit
import Foundation

class MainFlowCoordinatorDefault: NSObject, MainFlowCoordinator {
    let navigationController = UINavigationController()
    
    var getMovies: GetMoviesAction
    var getMusic: GetMusicAction
    var getTvShows: GetTvShowsAction
    
    override init() {
        let networkClient = ReactiveNetworkClient()
        let repository = ItunesSearchRepositoryApi(networkClient: networkClient)
        
        getMovies = GetMovies(repository: repository)
        getMusic = GetMusic(repository: repository)
        getTvShows = GetTvShows(repository: repository)
    }
    
    func loadGameMainFlowNavBarController() -> UINavigationController {
        navigationController.setViewControllers([createDashboardViewController()], animated: false)
        return self.navigationController
    }
    
    func presentMultimediaPreview(url: String) {
        let videoURL = URL(string: url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        navigationController.present(playerViewController, animated: true) {
            playerViewController.player?.play()
        }
    }
    
    private func createDashboardViewController() -> UIViewController {
        let presenter = DashboardPresenterDefault(getMovies: getMovies, getMusic: getMusic, getTvShows: getTvShows, coordinator: self)
        
        let view = DashboardViewController(presenter: presenter)
        presenter.view = view
        
        return view
    }
}
